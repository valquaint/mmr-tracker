// Images Sourced: https://www.deviantart.com/klydestorm/art/Majora-s-Mask-Icon-Pack-180392343

var ITEMS = [];
var dummyITEM = {
	img : [],
	ind : 0,
	name : "",
	counted : false,
	increments : [0,20,30,40],
	Clicked : function() {
		this.ind ++;
		if(this.ind>=this.img.length){
			this.ind = 0;
		}
		LOG("Now selected: "+this.name+" - "+this.img[this.ind],1);
		UpdateLogic(this.name,this.ind);
		return this.img[this.ind];
	},
	Init : function() {
		this.ind = 0;
		if(this.img.length==1){
			this.img[1] = this.img[0];
		}
		if(!ItemFlags[this.name]){
			ItemFlags.push(this.name);
		}
		return this.img[this.ind];
	}
};

function showhide(t){
	var elem = document.getElementsByClassName(t);

	for(var i = 0; i <= elem.length; i++){
		elem[i].setAttribute("style","display: "+(elem[i].getAttribute("style")=="display: none;" ? "block;" : "none;"));
	}
}
ItemList = ["Ocarina of Time","Bow","Fire Arrow","Ice Arrow","Light Arrow","Green Potion","Bombs","Bombchu","Deku Stick","Deku Nut","Magic Beans","Red Potion","Powder Keg","Pictograph Box","Lens of Truth","Hookshot","Great Fairys Sword","Blue Potion","Room Key","Pendant of Memories","Letter to Kafei","Special Delivery to Mama","Empty Bottle","Big Poe","Blue Fire","Bug","Chateau Romani","Deku Princess","Fairy","Fish","Gold Dust","Hot Spring Water","Hylian Loach","Magical Mushroom","Milk","Poe","Sea Horse","Spring Water","Zora Egg","Land Title Deed","Moons Tear","Mountain Title Deed","Ocean Title Deed","Swamp Title Deed"];
MaskList = ["Postmans Hat","All-Night Mask","Blast Mask","Stone Mask","Great Fairys Mask","Deku Mask","Keaton Mask","Bremen Mask","Bunny Hood","Don Geros Mask","Mask of Scents","Goron Mask","Romanis Mask","Circus Leaders Mask","Kafeis Mask","Couples Mask","Mask of Truth","Kamaros Mask","Gibdo Mask","Garos Mask","Captains Hat","Giants Mask","Zora Mask","Fierce Deitys Mask"];
MiscList = ["Sword","Shield","Wallet","Quiver","Bombers Notebook","Elegy of Emptiness","Eponas Song","Sonata of Awakening","Song of Healing","Song of Soaring","Song of Storms","Song of Time","Goron Lullaby","New Wave Bossa Nova","Oath to Order","Gyorgs Remains","Odolwas Remains","Gohts Remains","Twinmolds Remains"];


// TO BE SORTED

ITEMS["All-Night Mask"] = copy(dummyITEM);
ITEMS["All-Night Mask"].img = ["./img/All-Night Mask.png","./img/All-Night Mask.png"];

ITEMS["Big Poe"] = copy(dummyITEM);
ITEMS["Big Poe"].img = ["./img/Big Poe.png","./img/Big Poe.png"];

ITEMS["Blast Mask"] = copy(dummyITEM);
ITEMS["Blast Mask"].img = ["./img/Blast Mask.png","./img/Blast Mask.png"];

ITEMS["Blue Fire"] = copy(dummyITEM);
ITEMS["Blue Fire"].img = ["./img/Blue Fire.png","./img/Blue Fire.png"];

ITEMS["Blue Potion"] = copy(dummyITEM);
ITEMS["Blue Potion"].img = ["./img/Blue Potion.png","./img/Blue Potion.png"];

ITEMS["Bombs"] = copy(dummyITEM);
ITEMS["Bombs"].img = ["./img/Bomb Bag 20.png","./img/Bomb Bag 20.png","./img/Bomb Bag 30.png","./img/Bomb Bag 40.png"];
ITEMS["Bombs"].counted = true;

ITEMS["Bombchu"] = copy(dummyITEM);
ITEMS["Bombchu"].img = ["./img/Bombchu.png","./img/Bombchu.png"];

ITEMS["Bombers Notebook"] = copy(dummyITEM);
ITEMS["Bombers Notebook"].img = ["./img/Bombers Notebook.png","./img/Bombers Notebook.png"];

ITEMS["Bremen Mask"] = copy(dummyITEM);
ITEMS["Bremen Mask"].img = ["./img/Bremen Mask.png","./img/Bremen Mask.png"];

ITEMS["Bug"] = copy(dummyITEM);
ITEMS["Bug"].img = ["./img/Bug.png","./img/Bug.png"];

ITEMS["Bunny Hood"] = copy(dummyITEM);
ITEMS["Bunny Hood"].img = ["./img/Bunny Hood.png","./img/Bunny Hood.png"];

ITEMS["Captains Hat"] = copy(dummyITEM);
ITEMS["Captains Hat"].img = ["./img/Captains Hat.png","./img/Captains Hat.png"];

ITEMS["Chateau Romani"] = copy(dummyITEM);
ITEMS["Chateau Romani"].img = ["./img/Chateau Romani.png","./img/Chateau Romani.png"];

ITEMS["Circus Leaders Mask"] = copy(dummyITEM);
ITEMS["Circus Leaders Mask"].img = ["./img/Circus Leaders Mask.png","./img/Circus Leaders Mask.png"];

ITEMS["Couples Mask"] = copy(dummyITEM);
ITEMS["Couples Mask"].img = ["./img/Couples Mask.png","./img/Couples Mask.png"];

ITEMS["Deku Mask"] = copy(dummyITEM);
ITEMS["Deku Mask"].img = ["./img/Deku Mask.png","./img/Deku Mask.png"];

ITEMS["Deku Nut"] = copy(dummyITEM);
ITEMS["Deku Nut"].img = ["./img/Deku Nut.png","./img/Deku Nut.png"];

ITEMS["Deku Princess"] = copy(dummyITEM);
ITEMS["Deku Princess"].img = ["./img/Deku Princess.png","./img/Deku Princess.png"];

ITEMS["Deku Stick"] = copy(dummyITEM);
ITEMS["Deku Stick"].img = ["./img/Deku Stick.png","./img/Deku Stick.png"];

ITEMS["Don Geros Mask"] = copy(dummyITEM);
ITEMS["Don Geros Mask"].img = ["./img/Don Geros Mask.png","./img/Don Geros Mask.png"];

ITEMS["Elegy of Emptiness"] = copy(dummyITEM);
ITEMS["Elegy of Emptiness"].img = ["./img/Elegy of Emptiness.png","./img/Elegy of Emptiness.png"];

ITEMS["Empty Bottle"] = copy(dummyITEM);
ITEMS["Empty Bottle"].img = ["./img/Empty Bottle.png","./img/Empty Bottle.png","./img/Empty Bottle.png","./img/Empty Bottle.png","./img/Empty Bottle.png","./img/Empty Bottle.png","./img/Empty Bottle.png"];
ITEMS["Empty Bottle"].counted = true;
ITEMS["Empty Bottle"].increments = [0,1,2,3,4,5,6];

ITEMS["Eponas Song"] = copy(dummyITEM);
ITEMS["Eponas Song"].img = ["./img/Eponas Song (Edit).png","./img/Eponas Song (Edit).png"];

ITEMS["Fairy"] = copy(dummyITEM);
ITEMS["Fairy"].img = ["./img/Fairy.png","./img/Fairy.png"];

ITEMS["Fierce Deitys Mask"] = copy(dummyITEM);
ITEMS["Fierce Deitys Mask"].img = ["./img/Fierce Deitys Mask.png","./img/Fierce Deitys Mask.png"];

ITEMS["Fire Arrow"] = copy(dummyITEM);
ITEMS["Fire Arrow"].img = ["./img/Fire Arrow.png","./img/Fire Arrow.png"];

ITEMS["Fish"] = copy(dummyITEM);
ITEMS["Fish"].img = ["./img/Fish.png","./img/Fish.png"];

ITEMS["Garos Mask"] = copy(dummyITEM);
ITEMS["Garos Mask"].img = ["./img/Garos Mask.png","./img/Garos Mask.png"];

ITEMS["Giants Mask"] = copy(dummyITEM);
ITEMS["Giants Mask"].img = ["./img/Giants Mask.png","./img/Giants Mask.png"];

ITEMS["Wallet"] = copy(dummyITEM);
ITEMS["Wallet"].img = ["./img/Adults Wallet.png","./img/Adults Wallet.png","./img/Giants Wallet.png"];
ITEMS["Wallet"].counted = true;
ITEMS["Wallet"].increments = [0,500,2000];

ITEMS["Gibdo Mask"] = copy(dummyITEM);
ITEMS["Gibdo Mask"].img = ["./img/Gibdo Mask.png","./img/Gibdo Mask.png"];

ITEMS["Gohts Remains"] = copy(dummyITEM);
ITEMS["Gohts Remains"].img = ["./img/Gohts Remains.png","./img/Gohts Remains.png"];

ITEMS["Gold Dust"] = copy(dummyITEM);
ITEMS["Gold Dust"].img = ["./img/Gold Dust.png","./img/Gold Dust.png"];

ITEMS["Goron Lullaby"] = copy(dummyITEM);
ITEMS["Goron Lullaby"].img = ["./img/Goron Lullaby.png","./img/Goron Lullaby.png"];

ITEMS["Goron Mask"] = copy(dummyITEM);
ITEMS["Goron Mask"].img = ["./img/Goron Mask.png","./img/Goron Mask.png"];

ITEMS["Great Fairys Mask"] = copy(dummyITEM);
ITEMS["Great Fairys Mask"].img = ["./img/Great Fairys Mask.png","./img/Great Fairys Mask.png"];

ITEMS["Great Fairys Sword"] = copy(dummyITEM);
ITEMS["Great Fairys Sword"].img = ["./img/Great Fairys Sword.png","./img/Great Fairys Sword.png"];

ITEMS["Green Potion"] = copy(dummyITEM);
ITEMS["Green Potion"].img = ["./img/Green Potion.png","./img/Green Potion.png"];

ITEMS["Gyorgs Remains"] = copy(dummyITEM);
ITEMS["Gyorgs Remains"].img = ["./img/Gyorgs Remains.png","./img/Gyorgs Remains.png"];

ITEMS["Bow"] = copy(dummyITEM);
ITEMS["Bow"].img = ["./img/Heros Bow.png","./img/Heros Bow.png"];
ITEMS["Bow"].counted = false;

ITEMS["Hookshot"] = copy(dummyITEM);
ITEMS["Hookshot"].img = ["./img/Hookshot.png","./img/Hookshot.png"];

ITEMS["Hot Spring Water"] = copy(dummyITEM);
ITEMS["Hot Spring Water"].img = ["./img/Hot Spring Water.png","./img/Hot Spring Water.png"];

ITEMS["Hylian Loach"] = copy(dummyITEM);
ITEMS["Hylian Loach"].img = ["./img/Hylian Loach.png","./img/Hylian Loach.png"];

ITEMS["Ice Arrow"] = copy(dummyITEM);
ITEMS["Ice Arrow"].img = ["./img/Ice Arrow.png","./img/Ice Arrow.png"];

ITEMS["Kafeis Mask"] = copy(dummyITEM);
ITEMS["Kafeis Mask"].img = ["./img/Kafeis Mask.png","./img/Kafeis Mask.png"];

ITEMS["Kamaros Mask"] = copy(dummyITEM);
ITEMS["Kamaros Mask"].img = ["./img/Kamaros Mask.png","./img/Kamaros Mask.png"];

ITEMS["Keaton Mask"] = copy(dummyITEM);
ITEMS["Keaton Mask"].img = ["./img/Keaton Mask.png","./img/Keaton Mask.png"];

ITEMS["Sword"] = copy(dummyITEM);
ITEMS["Sword"].img = ["./img/Kokiri Sword.png","./img/Razor Sword.png","./img/Gilded Sword.png"];

ITEMS["Land Title Deed"] = copy(dummyITEM);
ITEMS["Land Title Deed"].img = ["./img/Land Title Deed.png","./img/Land Title Deed.png"];

ITEMS["Lens of Truth"] = copy(dummyITEM);
ITEMS["Lens of Truth"].img = ["./img/Lens of Truth.png","./img/Lens of Truth.png"];

ITEMS["Letter to Kafei"] = copy(dummyITEM);
ITEMS["Letter to Kafei"].img = ["./img/Letter to Kafei.png","./img/Letter to Kafei.png"];

ITEMS["Light Arrow"] = copy(dummyITEM);
ITEMS["Light Arrow"].img = ["./img/Light Arrow.png","./img/Light Arrow.png"];

ITEMS["Magic Beans"] = copy(dummyITEM);
ITEMS["Magic Beans"].img = ["./img/Magic Beans.png","./img/Magic Beans.png"];

ITEMS["Magical Mushroom"] = copy(dummyITEM);
ITEMS["Magical Mushroom"].img = ["./img/Magical Mushroom.png","./img/Magical Mushroom.png"];

ITEMS["Mask of Scents"] = copy(dummyITEM);
ITEMS["Mask of Scents"].img = ["./img/Mask of Scents.png","./img/Mask of Scents.png"];

ITEMS["Mask of Truth"] = copy(dummyITEM);
ITEMS["Mask of Truth"].img = ["./img/Mask of Truth.png","./img/Mask of Truth.png"];

ITEMS["Milk"] = copy(dummyITEM);
ITEMS["Milk"].img = ["./img/Milk.png","./img/Milk.png"];

ITEMS["Shield"] = copy(dummyITEM);
ITEMS["Shield"].img = ["./img/Heros Shield.png","./img/Mirror Shield.png"];

ITEMS["Moons Tear"] = copy(dummyITEM);
ITEMS["Moons Tear"].img = ["./img/Moons Tear.png","./img/Moons Tear.png"];

ITEMS["Mountain Title Deed"] = copy(dummyITEM);
ITEMS["Mountain Title Deed"].img = ["./img/Mountain Title Deed.png","./img/Mountain Title Deed.png"];

ITEMS["New Wave Bossa Nova"] = copy(dummyITEM);
ITEMS["New Wave Bossa Nova"].img = ["./img/New Wave Bossa Nova.png","./img/New Wave Bossa Nova.png"];

ITEMS["Oath to Order"] = copy(dummyITEM);
ITEMS["Oath to Order"].img = ["./img/Oath to Order.png","./img/Oath to Order.png"];

ITEMS["Ocarina of Time"] = copy(dummyITEM);
ITEMS["Ocarina of Time"].img = ["./img/Ocarina of Time.png","./img/Ocarina of Time.png"];

ITEMS["Ocean Title Deed"] = copy(dummyITEM);
ITEMS["Ocean Title Deed"].img = ["./img/Ocean Title Deed.png","./img/Ocean Title Deed.png"];

ITEMS["Odolwas Remains"] = copy(dummyITEM);
ITEMS["Odolwas Remains"].img = ["./img/Odolwas Remains.png","./img/Odolwas Remains.png"];

ITEMS["Pendant of Memories"] = copy(dummyITEM);
ITEMS["Pendant of Memories"].img = ["./img/Pendant of Memories.png","./img/Pendant of Memories.png"];

ITEMS["Pictograph Box"] = copy(dummyITEM);
ITEMS["Pictograph Box"].img = ["./img/Pictograph Box.png","./img/Pictograph Box.png"];

ITEMS["Poe"] = copy(dummyITEM);
ITEMS["Poe"].img = ["./img/Poe.png","./img/Poe.png"];

ITEMS["Postmans Hat"] = copy(dummyITEM);
ITEMS["Postmans Hat"].img = ["./img/Postmans Hat.png","./img/Postmans Hat.png"];

ITEMS["Powder Keg"] = copy(dummyITEM);
ITEMS["Powder Keg"].img = ["./img/Powder Keg.png","./img/Powder Keg.png"];

ITEMS["Quiver"] = copy(dummyITEM);
ITEMS["Quiver"].img = ["./img/Quiver 30.png","./img/Quiver 30.png","./img/Quiver 40.png","./img/Quiver 50.png"];
ITEMS["Quiver"].counted = true;
ITEMS["Quiver"].increments = [0,30,40,50];

ITEMS["Red Potion"] = copy(dummyITEM);
ITEMS["Red Potion"].img = ["./img/Red Potion.png","./img/Red Potion.png"];

ITEMS["Romanis Mask"] = copy(dummyITEM);
ITEMS["Romanis Mask"].img = ["./img/Romanis Mask.png","./img/Romanis Mask.png"];

ITEMS["Room Key"] = copy(dummyITEM);
ITEMS["Room Key"].img = ["./img/Room Key.png","./img/Room Key.png"];

ITEMS["Sea Horse"] = copy(dummyITEM);
ITEMS["Sea Horse"].img = ["./img/Sea Horse.png","./img/Sea Horse.png"];

ITEMS["Sonata of Awakening"] = copy(dummyITEM);
ITEMS["Sonata of Awakening"].img = ["./img/Sonata of Awakening.png","./img/Sonata of Awakening.png"];

ITEMS["Song of Healing"] = copy(dummyITEM);
ITEMS["Song of Healing"].img = ["./img/Song of Healing (Edit).png","./img/Song of Healing (Edit).png"];

ITEMS["Song of Soaring"] = copy(dummyITEM);
ITEMS["Song of Soaring"].img = ["./img/Song of Soaring (Edit).png","./img/Song of Soaring (Edit).png"];

ITEMS["Song of Storms"] = copy(dummyITEM);
ITEMS["Song of Storms"].img = ["./img/Song of Storms (Edit).png","./img/Song of Storms (Edit).png"];

ITEMS["Song of Time"] = copy(dummyITEM);
ITEMS["Song of Time"].img = ["./img/Song of Time (Edit).png","./img/Song of Time (Edit).png"];

ITEMS["Special Delivery to Mama"] = copy(dummyITEM);
ITEMS["Special Delivery to Mama"].img = ["./img/Special Delivery to Mama.png","./img/Special Delivery to Mama.png"];

ITEMS["Spring Water"] = copy(dummyITEM);
ITEMS["Spring Water"].img = ["./img/Spring Water.png","./img/Spring Water.png"];

ITEMS["Stone Mask"] = copy(dummyITEM);
ITEMS["Stone Mask"].img = ["./img/Stone Mask.png","./img/Stone Mask.png"];

ITEMS["Swamp Title Deed"] = copy(dummyITEM);
ITEMS["Swamp Title Deed"].img = ["./img/Swamp Title Deed.png","./img/Swamp Title Deed.png"];

ITEMS["Twinmolds Remains"] = copy(dummyITEM);
ITEMS["Twinmolds Remains"].img = ["./img/Twinmolds Remains.png","./img/Twinmolds Remains.png"];

ITEMS["Zora Egg"] = copy(dummyITEM);
ITEMS["Zora Egg"].img = ["./img/Zora Egg.png","./img/Zora Egg.png"];

ITEMS["Zora Mask"] = copy(dummyITEM);
ITEMS["Zora Mask"].img = ["./img/Zora Mask.png","./img/Zora Mask.png"];



function copy(mainObj) {
  let objCopy = {}; // objCopy will store a copy of the mainObj
  let key;

  for (key in mainObj) {
    objCopy[key] = mainObj[key]; // copies each property to the objCopy object
  }
  return objCopy;
}
function initialize(item){
	var name = item.getAttribute("name");
	item.parentNode.setAttribute("style","background: url('"+ITEMS[name].Init()+"') no-repeat; background-size: 64px 64px;");
	if(ITEMS[name].ind==0){
		item.parentNode.style.opacity=0.6;
	}else{
		item.parentNode.style.opacity=1;
	}
}
function Change(item){
	var name = item.getAttribute("name");
	LOG("Selected: "+name,1);
	var I = ITEMS[name];
	item.parentNode.setAttribute("style","background: url('"+I.Clicked()+"') no-repeat; background-size: 64px 64px;");
	if(I.ind==0){
		item.parentNode.style.opacity=0.6;
		if(I.counted){
			document.getElementById("DIV_"+name).innerHTML = "";
		}
	}else{
		item.parentNode.style.opacity=1;
		if(I.counted){
			document.getElementById("DIV_"+name).innerHTML = (I.ind > 0 ? I.increments[I.ind] : "");
		}
	}
}