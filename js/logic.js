//logic.js for Majora's Mask Randomizer
var Locations;
var ItemFlags = [];
function initLogic(){
    LOG("Loading Logic..",1);
    Locations = require('./json/location_data.json');
    Generate_Location_List();
}
function UpdateLogic(item, v){
    if(!ItemFlags[item]){
        ItemFlags.push(item);
    }
    LOG("Setting logic track for "+item+" to "+v,1);
    ItemFlags[item] = (v == 0 ? "false" : "true");
    Object.keys(Locations).forEach(function(k){
        var AllMet = true;
        Object.keys(Locations[k].Requirements).forEach(function(r){
            LOG("Comparing USER "+r+" ["+ItemFlags[r]+"] to LOGIC "+r+" ["+Locations[k].Requirements[r]+"]",1)
            if(Locations[k].Requirements[r]!=ItemFlags[r]){
                AllMet = false;
            }
            if(AllMet){
                document.getElementById("chest_"+k).setAttribute("style","color: green;");
            }else{
                document.getElementById("chest_"+k).setAttribute("style","color: red;");
            }
        });
    });
}
function Generate_Location_List(){
    var LocList = document.getElementById("Listings");
    /*for(var i = 0; i <= Locations.length; i++){
        LOG("Loaded logic for "+LocList[i].Name,1)
        LocList.innerHTML += "<div onClick='listItem(this)'>"+LocList[i].Name+"</div>";
    }*/
    Object.keys(Locations).forEach(function(k){
        LOG("Loaded logic for "+Locations[k].Name,1)
        LocList.innerHTML += "<div style='color: red;' id='chest_"+k+"' onClick='listItem(this)'>"+Locations[k].Name+"</div>";
    });
}
function listItem(loc){
    if(loc.getAttribute("style")=="color: red;"){
        return;
    }
    if(loc.getAttribute("style")=="color: green;"){
        loc.setAttribute("style","color: grey;");
    }else{
        loc.setAttribute("style","color: green;");
    }
}