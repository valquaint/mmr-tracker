function LOG(g,newline=0){
	var C = document.getElementById("Console");
	C.innerHTML+=((newline==1 ? "<br/>" : "" )+g);
	C.scrollTop = C.scrollHeight;
	
	}

	function init(){
		LOG("Initializing UI..",1);
		LOG("Loading ITEMS...",1);
		var i = 0;
		var cnt = ItemList.length;
		document.getElementById("ITEMTABLE").innerHTML = "<tr><td colspan=6 onclick='showhide(\"ItemList\")'><b>Items:</b></td></tr>";
		var ROW = "<tr class='ItemList'>";
		var TT = setInterval(function(){
			if(i<cnt){
				LOG("ADDING "+ItemList[i],1);
				ROW += "<td class='itemdiv'><div id='DIV_"+ItemList[i]+"' class='itemdivDATA' onclick='Change(this);' name='"+ItemList[i]+"'></div></td>";
				ITEMS[ItemList[i]].name = ItemList[i];
			i++;
			if(i%6==0){
				ROW+="</tr><tr class='ItemList'>";
			}
			}else{
				clearInterval(TT);
				ROW += "</tr>";
				document.getElementById("ITEMTABLE").innerHTML += ROW;
				init2();
			}
		},10);
		
	}
	function init2(){
		LOG("Loading MASKS...",1);
		var i = 0;
		var cnt = MaskList.length;
		document.getElementById("ITEMTABLE").innerHTML += "<tr><td colspan=6 onclick='showhide(\"MaskList\")'><b>Masks:</b></td></tr>";
		var ROW = "<tr class='MaskList'>";
		var TT = setInterval(function(){
			if(i<cnt){
				LOG("ADDING "+MaskList[i],1);
				ROW += "<td class='itemdiv'><div id='DIV_"+MaskList[i]+"' class='itemdivDATA' onclick='Change(this);' name='"+MaskList[i]+"'></div></td>";
				ITEMS[MaskList[i]].name = MaskList[i];
			i++;
			if(i%6==0){
				ROW+="</tr><tr class='MaskList'>";
			}
			}else{
				clearInterval(TT);
				ROW += "</tr>";
				document.getElementById("ITEMTABLE").innerHTML += ROW;
				init3();
			}
		},10);
		
	}
	function init3(){
		LOG("Loading MISC...",1);
		var i = 0;
		var cnt = MiscList.length;
		document.getElementById("ITEMTABLE").innerHTML += "<tr><td colspan=5 onclick='showhide(\"MiscList\")'><b>Miscellaneous:</b></td></tr>";
		var ROW = "<tr class='MiscList'>";
		var TT = setInterval(function(){
			if(i<cnt){
				LOG("ADDING "+MiscList[i],1);
				ROW += "<td class='itemdiv'><div id='DIV_"+MiscList[i]+"' class='itemdivDATA' onclick='Change(this);' name='"+MiscList[i]+"'></div></td>";
				ITEMS[MiscList[i]].name = MiscList[i];
			i++;
			if(i%5==0){
				ROW+="</tr><tr class='MiscList'>";
			}
			}else{
				clearInterval(TT);
				ROW += "</tr>";
				document.getElementById("ITEMTABLE").innerHTML += ROW;
				init4();
			}
		},10);
		
	}
function init4(){
	document.getElementById("ITEMTABLE").innerHTML += "</tr>";
	var x = document.getElementsByClassName("itemdivDATA");
	var i;
	for (i = 0; i < x.length; i++) {
		initialize(x[i]);
	}
	initLogic();
}
function passclick(t){
	var c = t.childNodes;
	Change(c[0]);
}